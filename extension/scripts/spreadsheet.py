import uno


def get_form(ctx):
    desktop = ctx.getValueByName("/singletons/com.sun.star.frame.theDesktop")
    doc = desktop.getCurrentComponent()
    sheet = doc.getSheets()[0]
    dp = sheet.getDrawPage()
    forms = dp.getForms()
    form = forms.getByName("Form")
    return form


def set_series(*args):
    statement = conn.prepareCommand("getSeries", 1)
    result = statement.executeQuery()
    series = []
    while result.next():
        series.append(result.getString(1))

    series_list = form.getByName("series")
    series_list.StringItemList = series
    series_list.DefaultSelection = (0,)


def set_car(*args):
    series_list = form.getByName("series")
    serie_index = series_list.SelectedItems[0]
    serie = series_list.StringItemList[serie_index]
    statement = conn.prepareCommand("getCars", 1)
    statement.setString(1, serie)
    result = statement.executeQuery()
    cars = []
    while result.next():
        cars.append(result.getString(1))

    car_list = form.getByName("car")
    car_list.StringItemList = cars
    car_list.DefaultSelection = (0,)


ctx = uno.getComponentContext()
sm = ctx.getServiceManager()

dbm = sm.createInstanceWithContext("com.sun.star.sdb.DatabaseContext", ctx)
db = dbm.getByName("motorsport")
conn = db.getConnection("", "")
form = get_form(ctx)

g_exportedScripts = (set_series, set_car)
