-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: motorsport
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `series` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base pr` double(10,6) DEFAULT NULL,
  PRIMARY KEY (`series`,`car`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES ('Formula 1 2020','Alfa Romeo',115.610010),('Formula 1 2020','Alphatauri',115.610010),('Formula 1 2020','Ferrari',115.810010),('Formula 1 2020','Haas',115.610010),('Formula 1 2020','McLaren',115.810010),('Formula 1 2020','Mercedes-AMG',116.010010),('Formula 1 2020','Racing Point',115.810010),('Formula 1 2020','Red Bull Racing',115.910010),('Formula 1 2020','Renault',115.810010),('Formula 1 2020','Williams',115.610010),('LMP1 2009','Aston Martin',73.420006),('LMP1 2009','Audi',73.360001);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crew`
--

DROP TABLE IF EXISTS `crew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crew` (
  `series` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `step` smallint(5) NOT NULL,
  `gold` int(10),
  `pr` float,
  PRIMARY KEY (`series`,`member`,`step`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crew`
--

LOCK TABLES `crew` WRITE;
/*!40000 ALTER TABLE `crew` DISABLE KEYS */;
INSERT INTO `crew` VALUES ('Formula 1 2020','driver',1,0,0),('Formula 1 2020','driver',2,30,0.437233),('Formula 1 2020','driver',3,42,0.437233),('Formula 1 2020','driver',4,61,0.437233),('Formula 1 2020','driver',5,87,0.437233),('Formula 1 2020','driver',6,124,0.437233),('Formula 1 2020','driver',7,177,0.437233),('Formula 1 2020','driver',8,253,0.437233),('Formula 1 2020','principal',1,0,0),('Formula 1 2020','principal',2,9,0.191368),('Formula 1 2020','principal',3,11,0.191368),('Formula 1 2020','principal',4,14,0.191368),('Formula 1 2020','principal',5,17,0.191368),('Formula 1 2020','principal',6,21,0.191368),('Formula 1 2020','principal',7,25,0.191368),('Formula 1 2020','principal',8,31,0.191368),('Formula 1 2020','principal',9,39,0.191368),('Formula 1 2020','principal',10,47,0.191368),('Formula 1 2020','principal',11,58,0.191368),('Formula 1 2020','principal',12,72,0.191368),('Formula 1 2020','principal',13,89,0.191368),('LMP1 2009','driver',1,0,0),('LMP1 2009','driver',2,20,0.414383),('LMP1 2009','driver',3,28,0.414383),('LMP1 2009','driver',4,40,0.414383),('LMP1 2009','driver',5,58,0.414383),('LMP1 2009','driver',6,82,0.414383),('LMP1 2009','driver',7,118,0.414383),('LMP1 2009','driver',8,168,0.414383),('LMP1 2009','principal',1,0,0),('LMP1 2009','principal',2,6,0.181373),('LMP1 2009','principal',3,7,0.181373),('LMP1 2009','principal',4,9,0.181373),('LMP1 2009','principal',5,11,0.181373),('LMP1 2009','principal',6,14,0.181373),('LMP1 2009','principal',7,17,0.181373),('LMP1 2009','principal',8,21,0.181373),('LMP1 2009','principal',9,26,0.181373),('LMP1 2009','principal',10,31,0.181373),('LMP1 2009','principal',11,39,0.181373),('LMP1 2009','principal',12,48,0.181373),('LMP1 2009','principal',13,59,0.181373);
/*!40000 ALTER TABLE `crew` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selection`
--

DROP TABLE IF EXISTS `selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selection` (
  `key` tinyint(1) NOT NULL,
  `series` varchar(100) COLLATE utf8mb4_unicode_ci,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selection`
--

LOCK TABLES `selection` WRITE;
/*!40000 ALTER TABLE `selection` DISABLE KEYS */;
INSERT INTO `selection` VALUES (0,'Formula 1 2020','driver');
/*!40000 ALTER TABLE `selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tunings`
--

DROP TABLE IF EXISTS `tunings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tunings` (
  `series` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `step` smallint(5) NOT NULL,
  `ms` int(10),
  `pr` float,
  PRIMARY KEY (`series`,`category`,`step`),
  KEY `index1` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tunings`
--

LOCK TABLES `tunings` WRITE;
/*!40000 ALTER TABLE `tunings` DISABLE KEYS */;
INSERT INTO `tunings` VALUES ('Formula 1 2020','acceleration',0,0,0),('Formula 1 2020','acceleration',1,109800,0.397782),('Formula 1 2020','acceleration',2,117900,0.397782),('Formula 1 2020','acceleration',3,122000,0.397782),('Formula 1 2020','acceleration',4,123600,0.397782),('Formula 1 2020','acceleration',5,124400,0.397782),('Formula 1 2020','acceleration',6,162600,0.397782),('Formula 1 2020','acceleration',7,260200,0.397782),('Formula 1 2020','acceleration',8,284600,0.397782),('Formula 1 2020','acceleration',9,325300,0.397782),('Formula 1 2020','acceleration',10,366000,0.397782),('Formula 1 2020','braking',0,0,0),('Formula 1 2020','braking',1,73200,0.265182),('Formula 1 2020','braking',2,78600,0.265182),('Formula 1 2020','braking',3,81300,0.265182),('Formula 1 2020','braking',4,82400,0.265182),('Formula 1 2020','braking',5,83000,0.265182),('Formula 1 2020','braking',6,108400,0.265182),('Formula 1 2020','braking',7,173500,0.265182),('Formula 1 2020','braking',8,189800,0.265182),('Formula 1 2020','braking',9,216900,0.265182),('Formula 1 2020','braking',10,244000,0.265182),('Formula 1 2020','cornering',0,0,0),('Formula 1 2020','cornering',1,93800,0.339767),('Formula 1 2020','cornering',2,100700,0.339767),('Formula 1 2020','cornering',3,104200,0.339767),('Formula 1 2020','cornering',4,105600,0.339767),('Formula 1 2020','cornering',5,106300,0.339767),('Formula 1 2020','cornering',6,138900,0.339767),('Formula 1 2020','cornering',7,222300,0.339767),('Formula 1 2020','cornering',8,243100,0.339767),('Formula 1 2020','cornering',9,277900,0.339767),('Formula 1 2020','cornering',10,312600,0.339767),('Formula 1 2020','top speed',0,0,0),('Formula 1 2020','top speed',1,82300,0.298332),('Formula 1 2020','top speed',2,88400,0.298332),('Formula 1 2020','top speed',3,91500,0.298332),('Formula 1 2020','top speed',4,92700,0.298332),('Formula 1 2020','top speed',5,93300,0.298332),('Formula 1 2020','top speed',6,122000,0.298332),('Formula 1 2020','top speed',7,195200,0.298332),('Formula 1 2020','top speed',8,213500,0.298332),('Formula 1 2020','top speed',9,244000,0.298332),('Formula 1 2020','top speed',10,274500,0.298332),('LMP1 2009','acceleration',1,96400,0.377189),('LMP1 2009','acceleration',2,103500,0.377189),('LMP1 2009','acceleration',3,107100,0.377189),('LMP1 2009','acceleration',4,108500,0.377189),('LMP1 2009','acceleration',5,109200,0.377189),('LMP1 2009','acceleration',6,142700,0.377189),('LMP1 2009','acceleration',7,228400,0.377189),('LMP1 2009','braking',1,64200,0.251457),('LMP1 2009','braking',2,69000,0.251457),('LMP1 2009','braking',3,71400,0.251457),('LMP1 2009','braking',4,72300,0.251457),('LMP1 2009','braking',5,72800,0.251457),('LMP1 2009','braking',6,95200,0.251457),('LMP1 2009','braking',7,152300,0.251457),('LMP1 2009','braking',8,166500,0.251457),('LMP1 2009','cornering',1,82300,0.322182),('LMP1 2009','cornering',2,88400,0.322182),('LMP1 2009','cornering',3,91400,0.322182),('LMP1 2009','cornering',4,92700,0.322182),('LMP1 2009','cornering',5,93300,0.322182),('LMP1 2009','cornering',6,121900,0.322182),('LMP1 2009','cornering',7,195100,0.322182),('LMP1 2009','cornering',8,213400,0.322182),('LMP1 2009','top speed',1,72300,0.28289),('LMP1 2009','top speed',2,77600,0.28289),('LMP1 2009','top speed',3,80300,0.28289),('LMP1 2009','top speed',4,81400,0.28289),('LMP1 2009','top speed',5,81900,0.28289),('LMP1 2009','top speed',6,107100,0.28289),('LMP1 2009','top speed',7,171300,0.28289),('LMP1 2009','top speed',8,187400,0.28289),('LMP1 2009','top speed',9,214100,0.28289),('LMP1 2009','top speed',10,240900,0.28289);
/*!40000 ALTER TABLE `tunings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-13 15:05:17
