import unohelper

from me.rr3 import XRR3CalculatorAddIn


class RR3CalculatorAddIn(unohelper.Base, XRR3CalculatorAddIn):
    def __init__(self, context):
        self.context = context
        sm = self.context.getServiceManager()

        dbm = sm.createInstanceWithContext("com.sun.star.sdb.DatabaseContext",
                                           self.context)
        db = dbm.getByName("motorsport")
        self.conn = db.getConnection("", "")

    def getMaxUpgrades(self, series):
        statement = self.conn.prepareCommand("MaxTunings", 1)
        statement.setString(1, series)
        result = statement.executeQuery()
        upgrades = {}
        while result.next():
            upgrades[result.getString(1)] = result.getInt(2)

        statement = self.conn.prepareCommand("MaxCrew", 1)
        statement.setString(1, series)
        result = statement.executeQuery()
        while result.next():
            upgrades[result.getString(1)] = result.getInt(2)
        return ((upgrades["top speed"], upgrades["acceleration"],
                 upgrades["braking"], upgrades["cornering"],
                 upgrades["driver"], upgrades["principal"]),)

    def getBasePr(self, series, car):
        statement = self.conn.prepareCommand("BasePr", 1)
        statement.setString(1, series)
        statement.setString(2, car)
        result = statement.executeQuery()
        if not result.next():
            print("no base pr found")
            return -1
        return result.getDouble(1)

    def getCost(self, series, upgrades):
        statement = self.conn.prepareCommand("TuningsPr", 1)
        for i in range(4):
            statement.setInt(i + 1, upgrades[0][i])
        statement.setString(5, series)
        result = statement.executeQuery()
        if not result.next():
            print("no tunings pr found")
            return ((),)
        ms = result.getDouble(2)

        statement = self.conn.prepareCommand("CrewPr", 1)
        for i in range(2):
            statement.setInt(i + 1, upgrades[0][i + 4])
        statement.setString(3, series)
        result = statement.executeQuery()
        if not result.next():
            print("no crew pr found")
            return ((),)
        gold = result.getDouble(2)
        return ((ms, gold),)

    def getPr(self, series, car, upgrades):
        pr = self.getBasePr(series, car)
        if pr < 0:
            return -1.0

        statement = self.conn.prepareCommand("TuningsPr", 1)
        for i in range(4):
            statement.setInt(i + 1, upgrades[0][i])
        statement.setString(5, series)
        result = statement.executeQuery()
        if not result.next():
            print("no tunings pr found")
            return ((),)
        pr += result.getDouble(1)

        statement = self.conn.prepareCommand("CrewPr", 1)
        for i in range(2):
            statement.setInt(i + 1, upgrades[0][i + 4])
        statement.setString(3, series)
        result = statement.executeQuery()
        if not result.next():
            print("no crew pr found")
            return ((),)
        pr += result.getDouble(1)
        return pr

    def calculateMinCostDatabase(self, series, car, min_u, max_u, target):
        base = self.getBasePr(series, car)
        if base < 0:
            return ((),)

        statement = self.conn.prepareCommand("CrewPr", 1)
        statement.setInt(1, min_u[0][4])
        statement.setInt(2, min_u[0][5])
        statement.setString(3, series)
        result = statement.executeQuery()
        if not result.next():
            print("no crew pr found")
            return ((),)
        crewpr = result.getDouble(1)
        increase = target - base - crewpr - 0.05

        statement = self.conn.prepareCommand("TuningsUpgradePath", 1)
        statement.setString(1, series)
        for i in range(4):
            statement.setInt(2 + 2 * i, max_u[0][i])
            statement.setInt(3 + 2 * i, min_u[0][i])
        statement.setDouble(10, increase)
        result = statement.executeQuery()
        if result.next():
            upgrades = [result.getInt(i) for i in range(1, 5)]
            upgrades.extend(min_u[0][4:])
            return (upgrades,)
        else:
            statement = self.conn.prepareCommand("TuningsPr", 1)
            for i in range(4):
                statement.setInt(i + 1, max_u[0][i])
            statement.setString(5, series)
            result = statement.executeQuery()
            if not result.next():
                print("no tunings pr found")
                return ((),)
            tuningspr = result.getDouble(1)
            increase = target - base - tuningspr - 0.05

            statement = self.conn.prepareCommand("CrewUpgradePath", 1)
            statement.setString(1, series)
            for i in range(2):
                statement.setInt(2 + 2 * i, max_u[0][i + 4])
                statement.setInt(3 + 2 * i, min_u[0][i + 4])
            statement.setDouble(6, increase)
            result = statement.executeQuery()
            if not result.next():
                print("no result found")
                return ((),)
            upgrades = list(max_u[0][:4])
            upgrades.extend([result.getInt(i) for i in range(1, 3)])
            return (upgrades,)

    def calculateMinCostFunction(self, a, m, increase):
        if a is None or m is None or increase is None:
            return ((0,),)
        return (self.calculateMinCost(a, m, increase)[0],)

    def calculateMinCost(self, a, m, increase, base=None, start_column=0):
        nb_of_upgrade_categories = len(a[0]) // 2
        # setup for the first iteration
        if base is None:
            base = [0] * nb_of_upgrade_categories

        # required pr increase is reached
        if increase <= 0:
            return base.copy(), 0

        # try one extra upgrade and calculate cheapest upgrade path with that
        # configuration return the cheapest (including the cost of the extra
        # upgrade)
        best_result = None

        for i in range(start_column, nb_of_upgrade_categories):

            # checks if this column of upgrades isn't maxed out yet
            if base[i] >= m[0][i]:
                continue

            # find the upgrade to try out
            cost = a[base[i]][2 * i]
            pr = a[base[i]][2 * i + 1]

            # update the upgrade path with the extra upgrade
            base[i] += 1

            # calculate the cheapest upgrade path using this extra upgrade
            result = self.calculateMinCost(a, m, increase - pr, base, i)
            if result is not None:
                (b, c) = result
                c += cost

                # if this result is better than any found before, save it
                if best_result is None or c < best_result[1]:
                    best_result = (b, c)

            # reset the upgrade path to the original state to test the next
            # upgrade
            base[i] -= 1

        return best_result


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    RR3CalculatorAddIn,
    "me.rr3.calculatorImpl",
    ("me.rr3.XRR3CalculatorAddIn",))
