import uno


def get_form(ctx, name="SelectForm"):
    desktop = ctx.getValueByName("/singletons/com.sun.star.frame.theDesktop")
    doc = desktop.getCurrentComponent()
    dp = doc.getDrawPage()
    forms = dp.getForms()
    form = forms.getByName(name)
    return form


def get_series(form):
    statement = conn.prepareCommand("getSeries", 1)
    result = statement.executeQuery()
    series = []
    while result.next():
        series.append(result.getString(1))
    return series


def set_series(*args):
    form = get_form(ctx, "SelectForm")
    series = get_series(form)
    if form.hasByName("addSeries"):
        add_series_box = form.getByName("addSeries")
        new_series = add_series_box.getText().getString()
        if new_series not in series:
            series_list = form.getByName("series")
            series.append(new_series)

    series_list = form.getByName("series")
    if series_list.StringItemList != series:
        series_list.StringItemList = series
        series_list.DefaultSelection = (0,)
        set_categories()


def get_categories(form):
    series_list = form.getByName("series")
    serie_index = series_list.SelectedItems[0]
    series = series_list.StringItemList[serie_index]
    if form.hasByName("category"):
        statement = conn.prepareCommand("getCategories", 1)
    else:
        statement = conn.prepareCommand("getMembers", 1)
    statement.setString(1, series)
    result = statement.executeQuery()
    categories = []
    while result.next():
        categories.append(result.getString(1))
    return categories


def set_categories(*args):
    form = get_form(ctx, "SelectForm")
    categories = get_categories(form)

    if form.hasByName("category"):
        category_list = form.getByName("category")
    elif form.hasByName("member"):
        category_list = form.getByName("member")
    else:
        return
    category_list.StringItemList = categories
    category_list.DefaultSelection = (0,)


def add_category(*args):
    form = get_form(ctx, "SelectForm")
    categories = get_categories(form)

    add_series_box = form.getByName("addCategory")
    new_category = add_series_box.getText().getString()
    if new_category not in categories:
        if form.hasByName("category"):
            category_list = form.getByName("category")
        else:
            category_list = form.getByName("member")
        categories.append(new_category)
        category_list.StringItemList = categories


ctx = uno.getComponentContext()
sm = ctx.getServiceManager()

dbm = sm.createInstanceWithContext("com.sun.star.sdb.DatabaseContext", ctx)
db = dbm.getByName("motorsport")
conn = db.getConnection("", "")
form = get_form(ctx, "SelectForm")
